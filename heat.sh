#!/bin/bash

# Download and untar sources
# git command is required
git clone https://gitlab.inria.fr/sed-bso/heat.git
cd heat/

# Configure, Make (with clang-sa analysis using scan-build)
mkdir -p build
cd build
# make clean
CFLAGS="--coverage -fPIC -fdiagnostics-show-option -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wmissing-prototypes -Wstrict-prototypes -Wcomment -pedantic -g"
LDFLAGS="--coverage"
cmake .. -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_C_FLAGS="$CFLAGS" -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS"
scan-build -v -plist --intercept-first --analyze-headers -o analyzer_reports make 2>&1 | tee heat-build.log

# Execute unitary tests (autotest)
ctest -V

# Collect coverage data
cd ..
lcov --directory . --capture --output-file heat.lcov
genhtml -o coverage heat.lcov
# see the result, for example: chromium-browser coverage/index.html
lcov_cobertura.py heat.lcov --output heat-coverage.xml

# Run cppcheck analysis
DEFINITIONS=""
CPPCHECK_INCLUDES="-I."
SOURCES_TO_EXCLUDE="-ibuild/CMakeFiles/"
SOURCES_TO_ANALYZE="."
cppcheck -v -f --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${DEFINITIONS} ${CPPCHECK_INCLUDES} ${SOURCES_TO_EXCLUDE} ${SOURCES_TO_ANALYZE} 2> heat-cppcheck.xml

# Run rats analysis


# Run vera++ analysis


# Run valgrind analysis
valgrind --xml=yes --xml-file=heat-valgrind.xml --memcheck:leak-check=full --show-reachable=yes "./build/heat_seq" "10" "10" "200" "0" "0"

# Create the config for sonar-scanner
sonar.host.url=https://sonarqube.inria.fr/sonarqube
sonar.links.homepage=https://gitlab.inria.fr/sed-bso/heat
sonar.links.scm=https://gitlab.inria.fr/sed-bso/heat.git
sonar.projectKey=sedbso:heat:gitlab:master
sonar.projectDescription=Solve the heat propagation equation
sonar.projectVersion=1.0
sonar.login=34539dd39d77f225f552676191a23ff725116702
sonar.scm.disabled=false
sonar.scm.provider=git
sonar.sourceEncoding=UTF-8
sonar.sources=.
sonar.exclusions=build/CMakeFiles/**
sonar.language=c
sonar.c.errorRecoveryEnabled=true
sonar.c.compiler.parser=GCC
sonar.c.includeDirectories=$(echo | gcc -E -Wp,-v - 2>&1 | grep "^ " | tr '\n')
sonar.c.compiler.charset=UTF-8
sonar.c.compiler.regex=^(.*):(\\\d+):\\\d+: warning: (.*)\\\[(.*)\\\]$
sonar.c.compiler.reportPath=build/heat-build.log
sonar.c.clangsa.reportPath=build/analyzer_reports/*/*.plist
sonar.c.coverage.reportPath=heat-coverage.xml
sonar.c.cppcheck.reportPath=heat-cppcheck.xml
sonar.c.valgrind.reportPath=heat-valgrind.xml

# Run the sonar-scanner analysis and submit to SonarQube server
sonar-scanner -X >sonar.log 2>&1
