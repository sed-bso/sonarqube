#!/bin/sh

# Install Git, GCC, CMake, Clang, GCOV, LCOV, Cppcheck, Valgrind, Python (+pip+pylint+test and coverage modules+scan-build for Clang SA)
sudo dnf upgrade -y
sudo dnf install -y git gcc gcc-gfortran gcc-c++ cmake clang clang-tools-extra gcovr lcov cppcheck valgrind python python-pip pylint

sudo pip install --upgrade pip
sudo python -m pip install pytest pytest-cov setuptools scan-build

./install_drmemory_unix.sh
./install_lcov_to_cobertura_unix.sh
